#!/bin/bash
set -exuo pipefail

VERSION=v1.4.0

rm -f kubevirt/chart/templates/*
rm -f kubevirt-crd/chart/templates/*

curl -fsSL -o kubevirt/chart/templates/kubevirt-operator.yaml \
    https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-operator.yaml
curl -fsSL -o kubevirt/chart/templates/kubevirt-cr.yaml \
    https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-cr.yaml

chart-prettier --stdin=false kubevirt/chart/templates
rm kubevirt/chart/templates/namespace.yaml
mv kubevirt/chart/templates/customresourcedefinition.yaml kubevirt-crd/chart/templates/customresourcedefinition.yaml
