#!/bin/bash
set -euxo pipefail

kubectl delete -f vm.yaml --wait --cascade=foreground --ignore-not-found=true
kubectl apply -f vm.yaml

until kubectl get po -l kubevirt.io=virt-launcher -l vm.kubevirt.io/name=testvm --no-headers | grep Running; do sleep 5; done

kubectl delete -f vm.yaml
