#!/bin/sh

set -eux

get_pod_ip(){
	kubectl get pod --selector=vm.kubevirt.io/name=ubuntu-test --output=jsonpath='{.items[0].status.podIP}'
}

kubectl delete secret ssh-public-key --ignore-not-found
kubectl delete secret ssh-private-key --ignore-not-found
kubectl delete --filename=vm-ubuntu-with-qemu-guest-agent.yaml --cascade=foreground --ignore-not-found --wait

ssh-keygen -f /tmp/id_rsa -C '' -N ''
truncate -s -1 /tmp/id_rsa.pub

kubectl create secret generic ssh-public-key --from-file=id_rsa.pub=/tmp/id_rsa.pub
kubectl create secret generic ssh-private-key --from-file=id_rsa=/tmp/id_rsa

kubectl get secret/ssh-private-key --output=jsonpath='{.data.id_rsa}' | base64 -d >/tmp/private.key
chmod 0600 /tmp/private.key

kubectl apply --filename=vm-ubuntu-with-qemu-guest-agent.yaml

MAX_LOOPS=10

LOOPS=0
until POD_IP="$(get_pod_ip)" && test -n "${POD_IP:-}"; do
	LOOPS=$((LOOPS+1))
	if test "${LOOPS:?}" -gt "${MAX_LOOPS:?}"; then exit 1; fi
	sleep 5
done

LOOPS=0
until nc -vz "${POD_IP:?}" 22; do
	LOOPS=$((LOOPS+1))
	if test "${LOOPS:?}" -gt "${MAX_LOOPS:?}"; then exit 1; fi
	sleep 10
done

LOOPS=0
# if the test is failing at the next line, check the logs of the virt-launcher for errors
until ssh -i /tmp/private.key -o 'StrictHostKeyChecking=no' -T "ubuntu@${POD_IP:?}"; do
	LOOPS=$((LOOPS+1))
	if test "${LOOPS:?}" -gt "${MAX_LOOPS:?}"; then exit 1; fi
	sleep 20
done
